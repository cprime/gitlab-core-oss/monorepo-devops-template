---
layout: default
title: Monorepo for Example Project
parent: Architectural Decision Record
nav_order: 1
---

# 2. Monorepo for Example Project

Date: 2021-10-26

## Status

Accepted

## Context

The issue motivating this decision, and any context that influences or constrains the decision.

## Decision

The change that we're proposing or have agreed to implement.

## Consequences

What becomes easier or more difficult to do and any risks introduced by the change that will need to be mitigated.
