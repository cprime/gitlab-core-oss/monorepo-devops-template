---
layout: default
title: Home
nav_order: 1
description: "A monorepo example project that demonstrates real-world DevOps practices and can act as a bootstrap template for new projects."
permalink: /
---

# Monorepo DevOps Playbook

## Introduction

Hi and welcome!

This repo represents a Cprime DevOps playbook. Within this repo, you will find a collection of lessons learned by us, the Cprime team. Playbooks are how we capture those lessons learned and share that knowledge amongst each other and the rest of the world. This repo is a template that contains our learning.

We are good at this. We are good at experimenting, learning, and sharing that information. These are skills that distinguish us as a market leader in what we do. It is the key to our success. We hire talented people who are brilliant at their job. We give them freedom and autonomy to learn new things through experimentation. We provide those people with collective memory. We learn together as a team.

This repo is not complete; it is a living thing. This repo will never be finished because there will always be new things to learn. Just as we at Cprime will never be finished, we are a living organization; we make discoveries every day.

Our playbooks cover different topics. Some focus on how we operate as a team, and others are product or technology specific. Our team writes all our playbooks, and all our playbooks describe the experience of our team. Playbooks do not contain hypotheses. We have hypotheses; we test them. If they prove to work, we capture the results in a playbook.

Playbooks are not intended to be prescriptive. They are not rule books. Do not follow the directions within this repo without question. We expect, we demand, that our team challenge and improve upon the contents of this or any other playbook.

The experience in this book is a gift to you from the team at Cprime. Take that collective experience and adapt it to your circumstances. In so doing, if you learn a new valuable lesson, please contribute back to this repo and the playbook.

We want to learn new lessons every day; we do not want to learn the same lessons and then forget them repeatedly. We capture our learning. We are Cprime; we have a collective memory; we are a team.
