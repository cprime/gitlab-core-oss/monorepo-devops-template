---
layout: default
title: Glossary
nav_order: 5
has_children: false
---

# Glossary

## Agile

A methodology for project and product management that is applied primarily to software development. Agile is characterized by the frequent releases of working product increments, iterative development and empirical process improvement.

### All Agile methodologies apply the principles of:

- The prioritization of individuals and interactions over processes and tools.
- The prioritization of working software over comprehensive documentation.
- The prioritization of customer collaboration over contract negotiation.
- The prioritization of responding to change over following a plan.

## Continuous Deployment (CD)

Continuous deployment is a practice for the release of software wherein any merge or commit to a given repository that successfully passes a Continuous Integration test suite is then deployed to a live environment automatically.

## CICD

Continuous Integration (CI), Continuous Deployment (CD).

## Continuous Integration (CI)

A practice of software development that requires developers to integrate code into version control frequently and in small increments. Each time code is pushed, merged or committed to that repository an automated build is triggered to verify code quality and product functionality against a suite of tests.

## Cohort

A group of people who all demonstrate a shared characteristic. Lean product development advocates dividing large groups of customers into smaller related groups for Cohort Analysis for the purpose of discovering potential product innovations.

## Definition of Done

A checklist of conditions that must be true for work delivered by a squad to be considered complete or 'Done.'  A squad may describe definitions for done at three different levels:

- Feature
- Sprint
- Release or Product Increment

## Definition of Ready

A checklist of conditions that must be true for work delivered by a squad to be considered ready to start.  A squad may describe definitions for done at three different levels:

- Feature
- Sprint
- Release or Product Increment

## Feature

A subset of overall product functionality that will deliver tangible value to a customer or end user.  Features may deliver an increment to existing product functionality or a new addition to product functionality. Features are the result of product delivery.

## Hypothesis

An idea for a customer value proposition that is based upon factual information and observed customer behaviour but has not been proven to be true.

## Individual Contributor

A term from the Spotify Model for Product Development. A person who works within Hello Fresh. Everyone in Hello Fresh is considered an individual contributor. Each individual makes their own contribution to the Hello Fresh product overall.

## Iteration

The act of repeatedly following the steps described in a squads work-flow to modify existing product features or deliver new product features to customers and subscribers until the conditions to prove a hypothesis for customer value have been validated.

## Kanban

A scheduling system originating from Japanese lean manufacturing and just-in-time manufacturing that has been adapted for agile software development. Kanban advocates explicitly limiting work in progress by establishing constraints to the amount of work-items permitted to enter a squad's work-flow within a given time-frame.

## KPI

Short for Key Performance Indicator. A metric to measure the performance of an organization or an activity performed by an organization.

## OKR

Short for Objectives and Key Results. A method for performance management developed by Intel and popularized by Google. The OKR method describes objectives as qualitative goals and progress towards those goals measured as quantitative key results.

## Product

An article or service that is manufactured with the intention of being sold. We make a clear distinction between digital products and physical products.

## Scrum

Scrum is a framework for developing, delivering, and maintaining products developed by Ken Shwaber and Jeff Sutherland. Scrum defines various roles, events, artifacts and rules. Scrum then defines a schedule for activities. Scrum is an Agile framework and advocates time-boxing as a means to limit work in progress.

## Scrumban

Scrumban is a framework for the development and operation of products and services developed by Corey Ladas. Scrumban applies Kanban and Lean methods for work visualisation and empirical process improvement. Scrumban advocates limiting work in progress by explicit constraints to a defined squad work-flow.

## Sprint

A practice prescribed by the Scrum Framework. A Sprint is a time-box of one month or less during which a “Done,” usable, and potentially releasable product Increment is created. Sprints have consistent durations throughout a development effort. A new Sprint starts immediately after the conclusion of the previous Sprint.

## Subscriber

A subscriber is a Hello Fresh customer with an active subscription for the Hello Fresh meal box service. In the U.S. we do not use the term 'subscription' we use the term 'plan.' In the U.S. a subscriber is a Hello Fresh customer who subscribes to a plan.

## Squad

A squad is a small cross-functional, self-organized agile team. The squad members have end-to-end responsibilities for product discovery and product delivery; they work together towards their mission and objectives.

Each Squad has the autonomy to decide what to build, how to build it, and how to work together while building it. The Squad itself ensures it remains aligned with the Squad mission, product vision, and quarterly objectives.

## Time-box

A practice of time management. Time-boxing allocates a fixed duration of time to a specified activity.

## Tribe

A tribe is a group of squads that are all aligned to a shared product vision. A tribe maintains high-level OKRs that are relative to all squads within that tribe. Squads will only ever belong to a single tribe.

## Work-flow

A sequence of steps involved in the product delivery process through which a feature or other item of work passes from initial idea to 'Done.' Work-flows involve two or more persons, and the combination of the activities within a work-flow are intended to create value propositions for Hello Fresh customers.

## XP

Short for Extreme Programming. XP is an agile software development methodology which takes an adaptive approach to the discovery and delivery of customer requirements. Kent Beck created XP as a result of working with the Chrysler organization. Kent Beck published XP as a recognized framework in 1999.
