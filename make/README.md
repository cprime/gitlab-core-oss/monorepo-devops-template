<a href="https://cprime.com/" target="_blank">
<img src="../../.assets/gitlab-logo-gray-rgb.png" width="400" />
</a>

[![Maintained by CPrime Elite Engineering](https://img.shields.io/badge/maintained%20by-cprime%20elite%20engineering-FC6D26)](https://cprime.com/)
[![Built for Engineers](https://img.shields.io/badge/project-monorepo%20devops%20template-FC6D26)](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template)
[![Latest](https://img.shields.io/badge/latest-0.0.0-FC6D26)](../../releases)
[![Chat on Gitter](https://img.shields.io/badge/community%20&%20support-chat%20on%20gitter-FC6D26)](https://gitter.im/cprime-elite-engineering/community?utm_source=share-link&utm_medium=link&utm_campaign=share-link)
# Make Directory

In this directory, we maintain a series of 'makefiles' that are executed using {GNU Make](https://www.gnu.org/software/make/).

# Build Status

[![pipeline status](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/badges/main/pipeline.svg)](https://gitlab.com/gitlab-dojo/gitlab-core-oss/monorepo-devops-template/-/pipelines)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

# Overview

Make was initially intended to control the generation of executables and other non-source files from the program's source files for the C and C++ programming languages. We extend this usage to encapsulate menial and repeatable engineering tasks in Make targets to reduce development time and ensure repeatability.

These make targets are primarily intended for use while developing code in the GitPod development environment. However, Make can also act as a helpful tool in CICD pipelines.

We document each makefile in this directory with code comments, and subdirectories do not contain an associated README file. This convention reduces unnecessary duplication of usage instructions.

The makefile at the root directory of this monorepo contains an include statement to include all make targets from files in the 'make' subdirectory.

``` make
#--------------------------------------------------------------------------------------------------------------
# General build variables
#--------------------------------------------------------------------------------------------------------------
export BUILD_PATH ?= $(shell 'pwd')

#--------------------------------------------------------------------------------------------------------------
# import make sub directories
#--------------------------------------------------------------------------------------------------------------
include $(BUILD_PATH)/make/*/makefile
```

Therefore, when calling make targets within this monorepo, you should execute the command from the root directory of this monorepo. If you are working within GitPod, this will be:

``` shell
/workspace/monorepo-devops-template
```


## License

We release all code within this repo under the MIT License. Please see [LICENSE](/LICENSE) and [NOTICE](/NOTICE) for more details.

<a href="https://cprime.com/" target="_blank">
<img src="../../.assets/dld-cp-sponsor.png" />
</a>
