#--------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------
FROM ubuntu:20.04

#--------------------------------------------------------------------------------------------------------------
# Install as root.
#--------------------------------------------------------------------------------------------------------------
USER root

# DEBIAN_FRONTEND=noninteractive ------------------------------------------------------------------------------
RUN export DEBIAN_FRONTEND=noninteractive

# Install git -------------------------------------------------------------------------------------------------
RUN pwd \
  && rm -rf /var/lib/apt/lists/* \
  && apt-get update \
  && apt-get install --no-install-recommends -y  git

# Install jekyll ----------------------------------------------------------------------------------------------
RUN pwd \
  && rm -rf /var/lib/apt/lists/* \
  && apt-get update \
  && apt-get install --no-install-recommends -y make build-essential \
  && apt-get install --no-install-recommends -y rubygems \
  && apt-get install --no-install-recommends -y ruby-dev \
  && bash -lc "gem install jekyll bundler just-the-docs"

# Install maven ----------------------------------------------------------------------------------------------
RUN apt-get install --no-install-recommends -y maven

# Install docker ---------------------------------------------------------------------------------------------
RUN pwd \
  && apt-get install --no-install-recommends -y  ca-certificates curl gnupg lsb-release \
  && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg \
  && echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null \
  && rm -rf /var/lib/apt/lists/* \
  && apt-get update \
  && apt-get install --no-install-recommends -y docker-ce docker-ce-cli containerd.io

# Install pre-commit ------------------------------------------------------------------------------------------
RUN pwd \
  && rm -rf /var/lib/apt/lists/* \
  && apt-get update \
  && apt-get install --no-install-recommends -y  python3-pip \
  && pip3 install setuptools \
  && pip3 install pre-commit \
  && pre-commit --version
